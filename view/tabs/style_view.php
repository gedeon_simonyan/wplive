<div class="live_content" data-id="3">
    <table>
        <tr valign="top">
            <th scope="row" style="width:55%;">

                <?php _e('Choose notification bar color', 'wpLive'); ?>

            </th>
            <td>
                <input type="text" class="wplive-color-picker regular-text" value="<?php echo $this->options->notsBGColor; ?>" id="wplive_notes_bg_color" name="wplive_notes_bg_color" placeholder="<?php _e('Example: #00FF00', 'wpLive'); ?>"/>
            </td>
        </tr>
        <tr valign="top">
            <th scope="row" style="width:55%;">

                <?php _e('Choose notification icons color', 'wpLive'); ?>

            </th>
            <td>
                <input type="text" class="wplive-color-picker regular-text" value="<?php echo $this->options->notsIconsColor; ?>" id="wplive_icons_bg_color" name="wplive_icons_bg_color" placeholder="<?php _e('Example: #00FF00', 'wpLive'); ?>"/>
            </td>
        </tr>
        <tr valign="top">
            <th scope="row" style="width:55%;">

                <?php _e('Choose notification text color', 'wpLive'); ?>

            </th>
            <td>
                <input type="text" class="wplive-color-picker regular-text" value="<?php echo $this->options->notsTextColor; ?>" id="wplive_text_color" name="wplive_text_color" placeholder="<?php _e('Example: #00FF00', 'wpLive'); ?>"/>
            </td>
        </tr>
        <tr valign="top">
            <th scope="row" style="width:55%;">

                <?php _e('Choose notification count text color', 'wpLive'); ?>

            </th>
            <td>
                <input type="text" class="wplive-color-picker regular-text" value="<?php echo $this->options->notsCountColor; ?>" id="wplive_count_text_color" name="wplive_count_text_color" placeholder="<?php _e('Example: #00FF00', 'wpLive'); ?>"/>
            </td>
        </tr>
        <tr valign="top">
            <th scope="row" style="width:55%;">

                <?php _e('Choose notification content background color', 'wpLive'); ?>

            </th>
            <td>
                <input type="text" class="wplive-color-picker regular-text" value="<?php echo $this->options->notsTextBGColor; ?>" id="wplive_text_bg_color" name="wplive_text_bg_color" placeholder="<?php _e('Example: #00FF00', 'wpLive'); ?>"/>
            </td>
        </tr>
        <tr valign="top">
            <th scope="row" style="width:55%;">

                <?php _e('Choose notification content background hover color', 'wpLive'); ?>

            </th>
            <td>
                <input type="text" class="wplive-color-picker regular-text" value="<?php echo $this->options->notsTextBGHoverColor; ?>" id="wplive_text_bg_hover_color" name="wplive_text_bg_hover_color" placeholder="<?php _e('Example: #00FF00', 'wpLive'); ?>"/>
            </td>
        </tr>
        <tr valign="top">
            <th scope="row" style="width:55%;">

                <?php _e('Choose notification title text colore', 'wpLive'); ?>

            </th>
            <td>
                <input type="text" class="wplive-color-picker regular-text" value="<?php echo $this->options->notsPostTitleColor; ?>" id="wplive_title_text_color" name="wplive_title_text_color" placeholder="<?php _e('Example: #00FF00', 'wpLive'); ?>"/>
            </td>
        </tr>

    </table>


</div>