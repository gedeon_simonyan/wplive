<div class="live_content" data-id="2">
    <?php
    $current_user_us = wp_get_current_user();
    $rolecurrent = $current_user_us->roles[0];

    $online_user_us = 'online_users_' . $rolecurrent;
    if ($this->options->option[$online_user_us]) {
        ?>
        <h3>View guests in Google Map</h3>
        <div id="map" style="height: 400px;width: 100%; "></div>

        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAdvxA4kkB8oQEOzSEZ7sjs2G7N6gl5WXc&callback=initMap">
        </script>
        <div style="margin: 20px">
            <input type="submit"  id="wplive_reset_map" value="<?php _e('Restart Map', 'wpLive'); ?>">
        </div>
    <?php } else { ?>
        <h3>No visitor at this moment.</h3>
    <?php } ?>
</div>