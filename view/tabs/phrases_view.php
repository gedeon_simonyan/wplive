<div class="live_content" data-id="4">
    <table>
        <tr valign="top">
            <th scope="row" style="width:55%;">

                <?php _e('Change comment title text', 'wpLive'); ?>

            </th>
            <td>
                <input type="text"  value="<?php echo $this->phrases->newComment; ?>" id="wplPhrase_new_comment" name="wplPhrase_new_comment" />
            </td>
        </tr>
        <tr valign="top">
            <th scope="row" style="width:55%;">

                <?php _e('Change comment text', 'wpLive'); ?>

            </th>
            <td>
                <input type="text"  value="<?php echo $this->phrases->commentText; ?>" id="wplPhrase_comment_text" name="wplPhrase_comment_text" />
            </td>
        </tr>
        <tr valign="top">
            <th scope="row" style="width:55%;">

                <?php _e('Change reply title text', 'wpLive'); ?>

            </th>
            <td>
                <input type="text"  value="<?php echo $this->phrases->newReply; ?>" id="wplPhrase_new_reply" name="wplPhrase_new_reply" />
            </td>
        </tr>
        <tr valign="top">
            <th scope="row" style="width:55%;">

                <?php _e('Change reply text', 'wpLive'); ?>

            </th>
            <td>
                <input type="text"  value="<?php echo $this->phrases->replyText; ?>" id="wplPhrase_reply_text" name="wplPhrase_reply_text" />
            </td>
        </tr>
        <tr valign="top">
            <th scope="row" style="width:55%;">

                <?php _e('Online guests title', 'wpLive'); ?>

            </th>
            <td>
                <input type="text"  value="<?php echo $this->phrases->onlineGuests; ?>" id="wplPhrase_online_guests" name="wplPhrase_online_guests" />
            </td>
        </tr>
        <tr valign="top">
            <th scope="row" style="width:55%;">

                <?php _e('Online users title', 'wpLive'); ?>

            </th>
            <td>
                <input type="text"  value="<?php echo $this->phrases->onlineUsers; ?>" id="wplPhrase_online_users" name="wplPhrase_online_users" />
            </td>
        </tr>
        <tr valign="top">
            <th scope="row" style="width:55%;">

                <?php _e('Change see all text', 'wpLive'); ?>

            </th>
            <td>
                <input type="text"  value="<?php echo $this->phrases->seeAll; ?>" id="wplPhrase_see_all" name="wplPhrase_see_all" />
            </td>
        </tr>
        <tr valign="top">
            <th scope="row" style="width:55%;">

                <?php _e('Change new user title', 'wpLive'); ?>

            </th>
            <td>
                <input type="text"  value="<?php echo $this->phrases->newUser; ?>" id="wplPhrase_new_user" name="wplPhrase_new_user" />
            </td>
        </tr>
        <tr valign="top">
            <th scope="row" style="width:55%;">

                <?php _e('Change new post added text', 'wpLive'); ?>

            </th>
            <td>
                <input type="text"  value="<?php echo $this->phrases->newPostAdded; ?>" id="wplPhrase_new_post_added" name="wplPhrase_new_post_added" />
            </td>
        </tr>
        <tr valign="top">
            <th scope="row" style="width:55%;">

                <?php _e('Change a few seconds ago text', 'wpLive'); ?>

            </th>
            <td>
                <input type="text"  value="<?php echo $this->phrases->fewSeconds; ?>" id="wplPhrase_few_second" name="wplPhrase_few_second" />
            </td>
        </tr> 
        <tr valign="top">
            <th scope="row" style="width:55%;">

                <?php _e('Show post view counts', 'wpLive'); ?>

            </th>
            <td>
                <input type="text"  value="<?php echo $this->phrases->views; ?>" id="wplPhrase_views" name="wplPhrase_views" />
            </td>
        </tr> 
        <tr valign="top">
            <th scope="row" style="width:55%;">

                <?php _e('Show post view counts now', 'wpLive'); ?>

            </th>
            <td>
                <input type="text"  value="<?php echo $this->phrases->viewsNow; ?>" id="wplPhrase_views_now" name="wplPhrase_views_now" />
            </td>
        </tr>

    </table>


</div>