<div class="live_content" data-id="1">
    <table>
        <tr valign="top">
            <th scope="row" style="width:55%;">

                <?php _e('Choose bar direction', 'wpLive'); ?>

                <p class="wpl-info"><?php _e('Chose bar direction on te screen', 'wpLive'); ?></p>
            </th>
            <td>
                <input type="radio" name="wpLive_vertical" <?php checked($this->options->verticalDirection == 'top'); ?> value="top" id="wpLive_bar_top"/>
                <label for="wpLive_bar_top">
                    <?php _e('Top', 'wpLive'); ?>
                </label><br>
                <input type="radio" name="wpLive_vertical" <?php checked($this->options->verticalDirection == 'bottom'); ?> value="bottom" id="wpLive_bar_bottom"/>
                <label for="wpLive_bar_bottom">
                    <?php _e('Bottom', 'wpLive'); ?>
                </label><br>
                <hr>
                <input type="radio" name="wpLive_horizontal" <?php checked($this->options->horizontalDirection == 'left'); ?> value="left" id="wpLive_bar_left"/>
                <label for="wpLive_bar_left">
                    <?php _e('Left', 'wpLive'); ?>
                </label><br>
                <input type="radio" name="wpLive_horizontal" <?php checked($this->options->horizontalDirection == 'right'); ?> value="right" id="wpLive_bar_right"/>
                <label for="wpLive_bar_right">
                    <?php _e('Right', 'wpLive'); ?>
                </label>
            </td>
        </tr>
        <tr>
            <th>
                <?php _e('Check user online interval', 'wpLive'); ?>
            </th>
            <td>
                <input type="number" name="wpLive_online_interval" min='3' value="<?php echo $this->options->interval; ?>" id="ucm_user_count"/>  <span style="display:inline-block; vertical-align:bottom; font-size:14px; padding:5px;"><?php _e('Seconds', 'wplive'); ?></span>
            </td>
        </tr>

        <tr>
            <th>
                <?php _e('Delete notification cache interval', 'wpLive'); ?>
            </th>
            <td>
                <input type="number" name="delete_cache" min='1' value="<?php echo $this->options->cacheInterval; ?>" id="ucm_user_count"/>  <span style="display:inline-block; vertical-align:bottom; font-size:14px; padding:5px;"><?php _e('Days', 'wplive'); ?></span>
            </td>
        </tr>
        <tr>
            <th>
                <label for="wpLive_view_count">
                    <?php _e('Show posts view count', 'wpLive'); ?>
                </label>
            </th>
            <td>
                <input type="checkbox" name="wpLive_view_count" <?php checked($this->options->showPostViewCount == 1); ?>  id="wpLive_view_count" value="1" />
            </td>
        </tr>

    </table>

</div>