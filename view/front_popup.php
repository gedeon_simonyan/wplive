<?php $rgbArr = $this->hex2rgb($this->options->notsTextBGColor);?>
<div class="wpl_popup" id="wpl_popup" style="height: 0;  <?php echo $this->options->verticalDirection?>: 30px;
<?php echo $this->options->horizontalDirection?>: 30px;">

    <script>
        function  getNewPosts(data) {
            
            var html="";
            if (data.newposts) {
                
                for(var i=1; i<=data.newposts.length; i++){
                    var h=i*100;
                    jQuery('#wpl_popup').animate({height: h+"px"});
                    var value=data.newposts[i-1];

                    html += "<div class='wpl_popup_inner' style='height: 100px;margin-bottom:5px; padding: 10px; background-color: rgba(<?php echo $rgbArr[0].','.$rgbArr[1].','.$rgbArr[2];?>, 0.85);'><div class='wpl_popup_content'><div class='wpl-new-post'><a href='" + value.postSlug + "'> ";
                    html += "<i class='fa fa-pencil-square-o' aria-hidden='true' style='color: <?php echo $this->options->notsIconsColor;?>'> </i>  <div class='new-post'><span style='color:<?php echo $this->options->notsTextColor;?>'> <span  class='wpl-comment-text' >" + value.postAuthor + " </span> <?php echo $this->phrases->newPostAdded;?> <span  style='color: <?php echo $this->options->notsBGColor;?>'> \"" + value.postTitle + "\" </span></span></div> <p style='color:<?php echo $this->options->notsTextColor;?>; font-size:10px'> <?php echo $this->phrases->fewSeconds;?> </p></a></div></div></div>";
                }
                var interval=<?php echo intval($this->options->interval);?>+'000';
                setTimeout(function () {
                    jQuery('#wpl_popup').animate({height: "0", padding:"0"});
                },interval);
            }

            if(data.current_comments){
                for(var j=1; j<=data.current_comments.length; j++){
                    var n=j*100;
                    jQuery('#wpl_popup').animate({height: n+"px"});
                    var val=data.current_comments[j-1];

                    html += "<div class='wpl_popup_inner' style='height: 100px;margin-bottom:5px; padding: 10px; background-color: rgba(<?php echo $rgbArr[0].','.$rgbArr[1].','.$rgbArr[2];?>, 0.85);'><div class='wpl_popup_content'><div class='wpl-new-post'><a href='" + val.link + "'> ";
                   if(val.message_type=="Reply"){
                       html += "<i class='fa fa-comment-o' aria-hidden='true' style='color: <?php echo $this->options->notsIconsColor;?>'> </i>  <div class='new-post'><span style='color:<?php echo $this->options->notsTextColor;?>'> <span  class='wpl-comment-text' >" + val.comment_author + " </span> <?php echo $this->phrases->replyText;?> <span  style='color: <?php echo $this->options->notsBGColor;?>'> \"" + val.post_title + "\" </span></span></div> <p style='color:<?php echo $this->options->notsTextColor;?>; font-size:10px'> <?php echo $this->phrases->fewSeconds;?> </p></a></div></div></div>";
                   }else{
                       html += "<i class='fa fa-comment-o' aria-hidden='true' style='color: <?php echo $this->options->notsIconsColor;?>'> </i>  <div class='new-post'><span style='color:<?php echo $this->options->notsTextColor;?>'> <span  class='wpl-comment-text' >" + val.comment_author + " </span> <?php echo $this->phrases->commentText;?> <span  style='color: <?php echo $this->options->notsBGColor;?>'> \"" + val.post_title + "\" </span></span></div> <p style='color:<?php echo $this->options->notsTextColor;?>; font-size:10px'> <?php echo $this->phrases->fewSeconds;?> </p></a></div></div></div>";

                   }
                }
                var int=<?php echo intval($this->options->interval);?>+'000';
                setTimeout(function () {
                    jQuery('#wpl_popup').animate({height: "0", padding:"0"});
                },int);
            }
            jQuery('.wpl_popup').html(html);
        }
    </script>
</div>