<span id="dubble_angle" class="wplive-button showbtn" title="Close Live Bar" style="background-color: <?php echo $this->options->notsBGColor; ?>">
        <span class='count-all' id='wpl_all_count'>0</span>
        <i class="fa fa-bell open" aria-hidden="true" style="color: <?php echo $this->options->notsIconsColor; ?>"></i>
    </span>
<div class="wplive_front_bar" style="background-color: <?php echo $this->options->notsBGColor; ?>">


    <style >
        .wpl-replay-item:hover, .wpl-comment-item:hover, .wpl-item:hover{
            background-color:<?php echo $this->options->notsTextBGHoverColor ?>!important;
        }
        .wpl-notes:hover{
            background-color:<?php echo $this->options->notsTextBGHoverColor ?>;
        }
        .wpl_content >div{
            border-bottom: 1px solid <?php echo $this->options->notsPostTitleColor; ?>;
        }
    </style>
    <?php $rgbArr = $this->hex2rgb($this->options->notsTextBGColor); ?>

    <?php if ($this->showNewComments) { ?>
        <div class='wpl-notes' title="<?php echo $this->phrases->newComment ?>">
            <div class="wpl_comment" >
                <div class='wpl_content' id='wpl_content_comment' style="background-color:transparent!important;">
                    <script>

                        function showCommentData(data) {
                            if (data !== null) {
                                var html = "";
                                var commnet_auther;
                                var post_title;
                                jQuery.each(data.comments, function (index, value) {
                                    commnet_auther = value.comment_author;
                                    post_title=value.post_title;
                                      
                                    if (commnet_auther.length > 20) {
                                        commnet_auther = commnet_auther.substring(0, 20) + "...";
                                    }
                                    
                                    if (post_title.length > 25) {
                                        post_title = post_title.substring(0, 25) + "...";
                                    }
                                    var message='';
                                    if(value.message_type=='Reply'){
                                        message=<?php echo '"'.$this->phrases->replyText.'"';?>;
                                    }else if(value.message_type=='Comment'){
                                        message=<?php echo '"'.$this->phrases->commentText.'"';  ?>;
                                    }

                                    var myDate=new Date(1000*value.date);
                                    myDate=myDate.toUTCString();

                                    html += "<div class='wpl-comment-item'  style=\"background-color: rgba(<?php echo $rgbArr[0] . ',' . $rgbArr[1] . ',' . $rgbArr[2]; ?>,0.95)\"><a href='" + value.link + "'> <i class='fa fa-comment-o' aria-hidden='true' style='color: <?php echo $this->options->notsIconsColor; ?>'> </i><span style='color:<?php echo $this->options->notsTextColor; ?>'><strong> " + commnet_auther + " </strong><?php /*_e($this->phrases->commentText, 'wplive');*/  ?> "+ message+"</span> <p class='wpl-comment-text' style='color:<?php echo $this->options->notsPostTitleColor; ?>'> \"" + post_title+ "\" </p><p style='font-size: 10px;'>"+ myDate+"</p> </a></div>";
                                });
                                jQuery('#wpl_content_comment').html(html);
                            }

                        }

                    </script>
                </div>
            </div>
            <i class="fa fa-comments wpl-icons  <?php if(get_user_meta(get_current_user_id(), 'wpl_comment')[0]=='1')
            { echo 'wpl-disabled';}?>" style="color: <?php echo $this->options->notsIconsColor.';';?>">
                <span class="wpl-count" id='wpl_commnets_count' style="color: <?php echo $this->options->notsTextColor; ?>; background-color: <?php echo $this->options->notsCountColor; ?>">0</span></i> </div>
    <?php } ?>

    <?php if ($this->showOnlineUsers) { ?>
        <div class='wpl-notes' title="<?php echo $this->phrases->onlineUsers ?>">
            <div class="wpl_online_users">
                <div class='wpl_content' id="wpl_users_content" style="background-color: transparent!important;">
                    <script>
                        function showOnlineUsers(data) {
                            if (data.users) {
                                var html = "";
                                jQuery.each(data.users.data, function (index, value) {
                                    html += " <div class='wpl-item' style=\"text-align: left; background-color: rgba(<?php echo $rgbArr[0] . ',' . $rgbArr[1] . ',' . $rgbArr[2]; ?>,0.85)\">";
                                    html += value['user_url'] ? "<a href='" + value['user_url'] + "'>" : '';
                                    html += "<i class='fa fa-user' style='color:<?php echo $this->options->notsIconsColor; ?>;font-size:20px; padding:15px' aria-hidden='true'></i><span style='color:<?php echo $this->options->notsTextColor; ?>'>" + value['display_name'] + "</span>";
                                    html += value['user_url'] ? "</a>" : '';
                                    html += "</div>";
                                });
                                jQuery('#wpl_users_content').html(html);
                            }
                        }
                    </script>
                </div>

            </div>

            <i class="fa fa-users  wpl-icons <?php if(get_user_meta(get_current_user_id(), 'wpl_online_users')[0]=='1')
            { echo 'wpl-disabled';}?>"
               style="color: <?php echo $this->options->notsIconsColor.';';?>">
                <span class="wpl-count" id='wpl_users' style="color: <?php echo $this->options->notsTextColor; ?>; background-color: <?php echo $this->options->notsCountColor; ?>">0</span></i> </div>
    <?php } ?>

    <div class='wpl-notes'  title="<?php echo $this->phrases->onlineGuests ?>">
        <div class="wpl_online_guests"></div>
        <i class="fa fa-user-secret  wpl-icons  <?php if(get_user_meta(get_current_user_id(), 'wpl_online_guests')[0]=='1')
        { echo 'wpl-disabled';}?>" style="color: <?php echo $this->options->notsIconsColor.';';?>" >
            <span class="wpl-count" id='wpl_guest' style="color: <?php echo $this->options->notsTextColor; ?>; background-color: <?php echo $this->options->notsCountColor; ?>">0</span></i> </div>

    <?php if ($this->showNewUsers) { ?>
        <div class='wpl-notes' title="<?php echo $this->phrases->newUser ?>">
            <div class="wpl_new_online_users">

                <div class='wpl_content' id="wpl_new_users_content" style="background-color: transparent!important;">
                    <script>
                        function showNewUsers(data) {
                            if (data !== null) {
                                var html = "";
                                var count = 0;
                                var display_name;
                                jQuery.each(data.newusers, function (index, value) {
                                    display_name = value.data.display_name;

                                    if (display_name.length > 20) {
                                        display_name = display_name.substring(0, 20) + "...";
                                    }

                                    html += " <div class='wpl-item' style=\"text-align: left; background-color: rgba(<?php echo $rgbArr[0] . ',' . $rgbArr[1] . ',' . $rgbArr[2]; ?>,0.85)\">";
                                    html += value['user_url'] ? "<a href='" + value.data.user_url + "'>" : '';
                                    html += "<i class='fa fa-user' style='color:<?php echo $this->options->notsIconsColor; ?>;font-size:20px; padding:15px' aria-hidden='true'></i><span style='color:<?php echo $this->options->notsTextColor; ?>'>" + display_name + "</span>";
                                    html += value['user_url'] ? "</a>" : '';
                                    html += "</div>";
                                    count++;
                                });
                                if (count > 0) {
                                    jQuery('#wpl_new_users_content').html(html);
                                    jQuery('#wpl_newuser').text(count);
                                    jQuery('#wpl_newuser').css('display', 'block')
                                } else {
                                    jQuery('#wpl_newuser').text(0);
                                    jQuery('#wpl_newuser').hide();
                                }
                            }
                        }


                    </script>
                </div>

            </div>




            <i class="fa fa-user-plus wpl-icons <?php if(get_user_meta(get_current_user_id(), 'wpl_new_online_users')[0]=='1')
            { echo 'wpl-disabled';}?>" style="color: <?php echo $this->options->notsIconsColor.';';?>">
                <span class="wpl-count" id='wpl_newuser' style="color: <?php echo $this->options->notsTextColor; ?>; background-color: <?php echo $this->options->notsCountColor; ?>">0</span></i>
        </div>
    <?php } ?>

</div>
