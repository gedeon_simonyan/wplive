<div class="options_tab">
    <form method="post"  name="wpLive_options_page" class="wplive_form" enctype="multipart/form-data">
        <ul class="admin_menu">
            <li class="active_tab" data-id="0">Permissions</li>
            <li data-id="1">Settings</li>
            <li data-id="2">map guests</li>
            <li data-id="3">Styles</li>
            <li data-id="4">Phrases</li>
        </ul>
        <div class="live_content" data-id="0">


            <table>
                <tbody>
                    <tr>
                        <th>Notes</th>
                        <?php foreach ($this->options->wpLiveRoles as $role): ?><th><?php echo$role ?></th><?php endforeach; ?>
                    </tr>
                    <?php foreach ($this->options->notesList as $list): ?>
                        <tr><td ><?php echo $list ?></td>
                            <?php foreach ($this->options->wpLiveRoles as $key => $val): ?>
                                <?php
                                foreach ($this->options->option as $opt_key => $opt) {
                                    $option_exist = strtolower(str_replace(' ', '_', $list)) . '_' . strtolower(str_replace(' ', '_', $val));
                                    if ($opt != null && $opt_key == $option_exist) {
                                ?>
                            <td><input type="checkbox" <?php checked($opt == 1); ?> name="<?php echo $option_exist; ?>"
                                       value="1"
                                    <?php if($val=='Guest' && $opt_key!='online_users_guest' && $val=='Guest' && $opt_key!='online_guests_guest' && $val=='Guest' && $opt_key!='new_post_guest' )
                                    {echo 'disabled';}
                                    ?> >
                                <?php
                                        }

                                    }
                                    ?><?php endforeach; ?></td>
                        </tr>

                    <?php endforeach; ?>

                </tbody>
            </table>

        </div>

        <?php include_once 'tabs/settings_view.php';?>
        <?php
            $roles = wp_get_current_user()->roles;
            if(is_array($roles)){            
                if(in_array('administrator',$roles)){
                    include_once 'tabs/map_view.php';
                }     
            }
        ?>
        <?php include_once 'tabs/style_view.php';?>
        <?php include_once 'tabs/phrases_view.php';?>

        <input type="submit" name="wpLiveSave" value="<?php _e('Save Options', 'wpLive'); ?>">
    </form>


</div>