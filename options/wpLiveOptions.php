<?php
/**
 * Created by PhpStorm.
 * User: GEDEON
 * Date: 08/10/2016
 * Time: 02:07 PM
 */

class wpLiveOptions{

    public $wpLiveRoles;
    public $notesList;
    public $option;
    public $settings;
    public $verticalDirection;
    public $horizontalDirection;
    public $liveOptionSlug="wpLivePermissions";
    public $liveSettingsSlug="wpLiveSettings";
    public $interval;
    public $showPostViewCount;
    public $cacheInterval;

    public $notsBGColor;
    public $notsIconsColor;
    public $notsTextColor;
    public $notsCountColor;
    public $notsTextBGColor;
    public $notsTextBGHoverColor;
    public $notsPostTitleColor;

    private  $dbprefix;

    public function __construct(){
        global $wpdb;
        $this->dbprefix = $wpdb->prefix;
        $this->getRoles();
        $this->getNotesList();
        $this->addOption();
        $this->initOption();
        if(isset($_POST['wpLiveSave'])){
            $this->saveOptions($_POST);
            $this->initOption();
        }

    }
    public function getInterval(){
        return $this->interval;
    }

    public function getRoles(){
        $roles_arr=get_option($this->dbprefix.'user_roles');
        $roles=array();
        foreach ($roles_arr as $role){
            $name=$role['name'];
            $roles[]=$name;

        }
        array_push($roles,'Guest');
        $this->wpLiveRoles=$roles;

    }

    public function getNotesList(){

        $noteList=array('Online users','Online guests','New post','New comment','New registration','Profile update',);
        $this->notesList=$noteList;
    }
    

    public function addOption(){
        $options=array();
        foreach ($this->notesList as $note) {
            $note=strtolower(str_replace(' ','_',$note));
            foreach ($this->wpLiveRoles as $roles) {
                $roles=strtolower(str_replace(' ','_',$roles));
                $tmp=$note.'_'.$roles;
                $options[$tmp]='0';
            }
        }
        $settings=array(
            'verticalDirection' => 'top',
            'horizontalDirection' => 'left',
            'onlineInterval' => 15,
            'cacheInterval' => 30,
            'showPostViewCount' => '1',
            'notsBGColor' => '#00a0d2',
            'notsIconsColor' => '#ffffff',
            'notsTextColor' => '#ffffff',
            'notsCountColor' => '#FF0000',
            'notsTextBGColor' => '#000000',
            'notsTextBGHoverColor' => '#1E1E20',
            'notsPostTitleColor' => '#ffffff',
            );
        add_option($this->liveOptionSlug, $options);
        add_option($this->liveSettingsSlug, $settings);

        $this->option=$options;
        $this->settings=$settings;

    }

    public function initOption(){
        $options = get_option($this->liveOptionSlug);
        $settings = get_option($this->liveSettingsSlug);
        $this->option=$options;
        $this->cacheInterval=$settings['cacheInterval'];
        $this->verticalDirection=$settings['verticalDirection'];
        $this->horizontalDirection=$settings['horizontalDirection'];
        $this->interval=$settings['onlineInterval'];
        $this->showPostViewCount=$settings['showPostViewCount'];
        $this->notsBGColor=$settings['notsBGColor'];
        $this->notsIconsColor=$settings['notsIconsColor'];
        $this->notsTextColor=$settings['notsTextColor'];
        $this->notsCountColor=$settings['notsCountColor'];
        $this->notsTextBGColor=$settings['notsTextBGColor'];
        $this->notsTextBGHoverColor=$settings['notsTextBGHoverColor'];
        $this->notsPostTitleColor=$settings['notsPostTitleColor'];
    }
    
    public function saveOptions($args) {

        $options = get_option($this->liveOptionSlug);

        $this->interval=isset($args['wpLive_online_interval']) ? intval($args['wpLive_online_interval']) : 15;
        $this->cacheInterval=isset($args['delete_cache']) ? intval($args['delete_cache']) : 30;
        $this->verticalDirection=isset($args['wpLive_vertical']) ? $args['wpLive_vertical'] : 'top';
        $this->horizontalDirection=isset($args['wpLive_horizontal']) ? $args['wpLive_horizontal'] : 'left';
        $this->showPostViewCount=isset($args['wpLive_view_count']) ? intval($args['wpLive_view_count']) : 0;
        $this->notsBGColor=isset($args['wplive_notes_bg_color']) ? $args['wplive_notes_bg_color'] : '#00a0d2';
        $this->notsIconsColor=isset($args['wplive_icons_bg_color']) ? $args['wplive_icons_bg_color'] : '#ffffff';
        $this->notsTextColor=isset($args['wplive_text_color']) ? $args['wplive_text_color'] : '#ffffff';
        $this->notsCountColor=isset($args['wplive_count_text_color']) ? $args['wplive_count_text_color'] : '#FF0000';
        $this->notsTextBGColor=isset($args['wplive_text_bg_color']) ? $args['wplive_text_bg_color'] : '#000000';
        $this->notsTextBGHoverColor=isset($args['wplive_text_bg_hover_color']) ? $args['wplive_text_bg_hover_color'] : '#1E1E20';
        $this->notsPostTitleColor=isset($args['wplive_title_text_color']) ? $args['wplive_title_text_color'] : '#ffffff';

        foreach ($options as $key=>$opt){
            $options[$key] =isset($args[$key]) ? intval($args[$key]):'0';

        }
        $settings=array(
            'verticalDirection' => $this->verticalDirection,
            'horizontalDirection' => $this->horizontalDirection,
            'onlineInterval' => $this->interval,
            'cacheInterval' => $this->cacheInterval,
            'showPostViewCount' => $this->showPostViewCount,
            'notsBGColor' => $this->notsBGColor,
            'notsIconsColor' => $this->notsIconsColor,
            'notsTextColor' => $this->notsTextColor,
            'notsCountColor' => $this->notsCountColor,
            'notsTextBGColor' => $this->notsTextBGColor,
            'notsTextBGHoverColor' => $this->notsTextBGHoverColor,
            'notsPostTitleColor' => $this->notsPostTitleColor,
        );
        update_option($this->liveOptionSlug,$options);
        update_option($this->liveSettingsSlug,$settings);

    }
   

}