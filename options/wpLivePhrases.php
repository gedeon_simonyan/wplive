<?php

/**
 * Created by PhpStorm.
 * User: GEDEON
 * Date: 12-Nov-16
 * Time: 03:46 PM
 */
class wpLivePhrases{

    public $livePhrasesSlug="wpLivePhrases";
    public $phrases;
    public $newComment;
    public $commentText;
    public $newReply;
    public $replyText;
    public $onlineGuests;
    public $onlineUsers;
    public $seeAll;
    public $newUser;
    public $newRegistration;
    public $newPostAdded;
    public $fewSeconds;
    public $views;
    public $viewsNow;

    public function __construct()
    {
        $this->addPhrases();
        $this->initPhrase();
        if (isset($_POST['wpLiveSave'])) {
            $this->savePhrase($_POST);
            $this->initPhrase();
        }
    }

    public function addPhrases(){
        $phrases=array(
            'newComment'=>'New comments',
            'commentText'=>'comment to your post',
            'newReply'=>'New reply',
            'replyText'=>'replied to your comment in a post',
            'onlineGuests'=>'Online guests count',
            'onlineUsers'=>'Online users',
            'seeAll'=>'See all',
            'newUser'=>'New Users',
            'newPostAdded'=>'added a new post',
            'fewSeconds'=>'a few second ago',
            'views'=>'Views',
            'viewsNow'=>'Views Now',
        );
        add_option($this->livePhrasesSlug,$phrases);
    }

    public function initPhrase(){
        $phrases = get_option($this->livePhrasesSlug);
        $this->phrases=$phrases;
        $this->newComment=$phrases['newComment'];
        $this->commentText=$phrases['commentText'];
        $this->newReply=$phrases['newReply'];
        $this->replyText=$phrases['replyText'];
        $this->onlineGuests=$phrases['onlineGuests'];
        $this->onlineUsers=$phrases['onlineUsers'];
        $this->seeAll=$phrases['seeAll'];
        $this->newUser=$phrases['newUser'];
        $this->newPostAdded=$phrases['newPostAdded'];
        $this->fewSeconds=$phrases['fewSeconds'];
        $this->views=$phrases['views'];
        $this->viewsNow=$phrases['viewsNow'];
    }

    public function savePhrase($args) {
        
        $this->replyText=isset($args['wplPhrase_reply_text']) ? addslashes($args['wplPhrase_reply_text']) :'replied to your comment in';
        $this->newComment=isset($args['wplPhrase_new_comment']) ? addslashes($args['wplPhrase_new_comment']) :'New comments';
        $this->commentText=isset($args['wplPhrase_comment_text']) ? addslashes($args['wplPhrase_comment_text']) : 'comment to your post';
        $this->newReply=isset($args['wplPhrase_new_reply']) ? addslashes($args['wplPhrase_new_reply']) : 'New reply';
        $this->onlineGuests=isset($args['wplPhrase_online_guests']) ? addslashes($args['wplPhrase_online_guests']) : 'Online guests count';
        $this->onlineUsers=isset($args['wplPhrase_online_users']) ? addslashes($args['wplPhrase_online_users']) : 'Online users';
        $this->seeAll=isset($args['wplPhrase_see_all']) ? addslashes($args['wplPhrase_see_all']) : 'See all';
        $this->newUser=isset($args['wplPhrase_new_user']) ? addslashes($args['wplPhrase_new_user']) : 'New user registered';
        $this->newPostAdded=isset($args['wplPhrase_new_post_added']) ? addslashes($args['wplPhrase_new_post_added']) : 'added a new post';
        $this->fewSeconds=isset($args['wplPhrase_few_second']) ? addslashes($args['wplPhrase_few_second']) : 'a few second ago';
        $this->views=isset($args['wplPhrase_views']) ? addslashes($args['wplPhrase_views']) : 'Views';
        $this->viewsNow=isset($args['wplPhrase_views_now']) ? addslashes($args['wplPhrase_views_now']) : 'Views Now';


        $phrases=array(
            'commentText' => $this->commentText,
            'newReply' => $this->newReply,
            'replyText' => $this->replyText,
            'newComment' => $this->newComment,
            'onlineGuests' => $this->onlineGuests,
            'onlineUsers' => $this->onlineUsers,
            'seeAll' => $this->seeAll,
            'newUser' => $this->newUser,
            'newPostAdded' => $this->newPostAdded,
            'fewSeconds' => $this->fewSeconds,
            'views' => $this->views,
            'viewsNow' => $this->viewsNow,

        );
        update_option($this->livePhrasesSlug,$phrases);

    }


}