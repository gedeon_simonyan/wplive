<?php

/*
 * Plugin Name: Wp Live
 * Description: Ad
 * Version: 1.0.0
 * Author: 
 * Contributors: 
 * Author URI: http://gvectors.com/
 * Plugin URI: http://gvectors.com/product/wpLive/
 * Text Domain: wpLive
 * Domain Path: /languages/
 */

include_once 'includes/wpLiveDbManager.php';
include_once 'options/wpLiveOptions.php';
include_once 'options/wpLivePhrases.php';
include(ABSPATH . "wp-includes/pluggable.php");

class wpLive {

    public $dbLive;
    public $options;
    public $phrases;
    private $online_date;
    private $userId;
    private $auther;
    private $wplVersion;
    public $showOnlineUsers = 0;
    public $showNewComments = 0;
    public $showNewUsers = 0;
    private $wpLive_data = array();

    public function __construct() {

        $this->options = new wpLiveOptions();
        $this->phrases = new wpLivePhrases();
        $this->dbLive = new wpLiveDbManager();
        define('LIVE_DIR_PATH', dirname(__FILE__));
        define('LIVE_DIR_NAME', basename(LIVE_DIR_PATH));
        $this->initPluginVersion();
        add_action('plugins_loaded', array(&$this, 'pluginsLoaded'), 14.5);
        add_action('wp_footer', array($this, 'showNotesPopup'), 11.2);
        add_action('admin_footer', array(&$this, 'showNotesPopup'), 15);
        add_action('admin_enqueue_scripts', array($this, 'registerLiveAdminStyle'), 111);
        add_action('wp_enqueue_scripts', array($this, 'registerLiveFrontStyle'));
        add_action('wp_ajax_nopriv_getWpLiveData', array($this, 'getWpLiveData'));
        add_action('wp_ajax_getWpLiveData', array($this, 'getWpLiveData'));
        add_action('admin_menu', array(&$this, 'addWpLiveMenu'));
        add_action('publish_post', array($this, 'addedNewPosts'), 10, 2);
        add_action('wp_ajax_commentSeen', array($this, 'commentSeen'));
        add_action('user_register', array($this, 'userRegistretion'), 51, 1);
        add_action('profile_update', array($this, 'userRegistretion'), 52, 1);
        add_action('profile_update', array($this, 'userProfileUpdate'), 52, 1);
        add_action('transition_comment_status', array(&$this, 'getNewPopupComments'), 44, 3);
        // Remove issues with prefetching adding extra views
        remove_action('wp_head', array($this, 'adjacent_posts_rel_link_wp_head'), 10);
        add_filter('the_content', array($this, 'my_the_post_action'));
        $this->setOnlineDate(get_current_user_id(), time());
        if (!is_user_logged_in()) {

            $this->dbLive->insertLiveGuests($this->get_guests_ip(), time());
            $this->dbLive->updateLiveGuests($this->get_guests_ip(), time());
        }
        $this->getAllRoles();
        add_action('wp_insert_comment', array($this, 'wpLiveNewPostComment'), 99, 2);
    }

    private function initPluginVersion() {
        if (!function_exists('get_plugins')) {
            require_once ABSPATH . 'wp-admin/includes/plugin.php';
        }
        $plugin_folder = get_plugins('/' . plugin_basename(dirname(__FILE__)));
        $plugin_file = basename((__FILE__));
        $this->wplVersion = $plugin_folder[$plugin_file]['Version'];
    }

    public function getNotifications() {

        if (isset($_POST['wplive_note_type'])) {
            $autherId = get_current_user_id();
            $key_exists = get_user_meta($autherId, $_POST['wplive_note_type']);
            if ($key_exists) {

                if ($key_exists[0] == '1') {
                    update_user_meta($autherId, $_POST['wplive_note_type'], 0);
                } else {
                    update_user_meta($autherId, $_POST['wplive_note_type'], 1);
                }
            } else {
                add_user_meta($autherId, $_POST['wplive_note_type'], 0);
            }
        }
    }

    /* Getting data for ajax requests */

    function getAllRoles() {
        $current_user = wp_get_current_user();
        $roles = $current_user->roles;
        $userID = get_current_user_id();
        $is_guest = '0';
        if (!is_user_logged_in()) {
            $userID = $this->get_guests_ip();
            $is_guest = '1';
        }

        if (!$roles) {
            $roles = array('guest');
        }

        $postID = isset($_POST['wpl_post_id']) ? $_POST['wpl_post_id'] : '';
        $this->setLiveViews($postID, $userID, $is_guest);
        $this->getPostViews($postID);
        $this->getNotifications();

        $showComments=get_user_meta($userID, 'wpl_comment');
        $showNewUsers=get_user_meta($userID, 'wpl_new_online_users');
        $showGuests=get_user_meta($userID, 'wpl_online_guests');
        $ShowUsers=get_user_meta($userID, 'wpl_online_users');

        foreach ($roles as $role) {
            $new_post = 'new_post_' . $role;
            $online_user = 'online_users_' . $role;
            $online_guests = 'online_guests_' . $role;
            $new_comment = 'new_comment_' . $role;
            $new_registration = 'new_registration_' . $role;
            $update_profile = 'profile_update_' . $role;

            if ($this->options->option[$new_post] == 1) {
                $this->getNewPosts();
            }

            if ($this->options->option[$new_comment] == 1) {
                $this->showNewComments = 1;
                if( $showComments[0]=='0'){
                    $this->getNewComments();
                    $this->getCurrentComments();
                }

            }

            if ($this->options->option[$online_user] == 1) {
                $this->showOnlineUsers = 1;
                if($ShowUsers[0]=='0') {
                    $this->getOnlineUsers();
                }
            }

            if ($this->options->option[$online_guests] == 1) {
                if($showGuests[0]=='0'){
                    $this->getOnlineGuests();
                }

            }

            if ($this->options->option[$new_registration] == 1) {
                $this->showNewUsers = 1;
                if($showNewUsers[0]=='0'){
                    $this->getNewUser();
                }

            }
            if ($this->options->option[$update_profile] == 1) {
                $this->getUserProfileUpdate();
            }
            if ($role == 'administrator') {
                $this->getOnlineIps();
            }
        }
    }

    function getWpLiveData() {
        $this->getAllRoles();

        if (!empty($this->wpLive_data)) {
            wp_die(json_encode($this->wpLive_data));
        } else {
            wp_die();
        }
    }

    /* Adding new post to the live_message table */

    function addedNewPosts($ID, $post) {
        $auther = $post->post_author;
        $this->dbLive->addNewPosts($ID, $auther);
    }

    /* Sending data about  new posts for  notification */

    function getNewPosts() {
        $this->dbLive->deleteOldPosts();
        $autherId = get_current_user_id();
        $postsId = $this->dbLive->getNewPosts($autherId);
        $i = 0;
        foreach ($postsId as $id) {
            $post = get_post($id['post_id']);
            if ($post) {
                $params[$i]['postTitle'] = $post->post_title;
                $params[$i]['postSlug'] = get_permalink($post->ID);
                $author = get_userdata($post->post_author);
                $params[$i]['postAuthor'] = $author->display_name;
                $wpLive_data['newposts'] = $params;
                $this->setWpLiveData($wpLive_data);
                $i++;
            }
        }
    }

    public function getNewPopupComments($newStatus, $oldStatus, $comment_data) {
        if ($newStatus != $oldStatus) {
            if ($newStatus == 'approved') {
                $this->dbLive->updateCommentDate($comment_data->comment_ID);
                return $newStatus;
            }
        }
    }

    public function getCurrentComments() {
        $comments = $this->dbLive->getUsersCurrentComments(get_current_user_id());

        if ($comments) {
            foreach ($comments as $wpl_key => $wpl_value) {
                if (isset($comments[$wpl_key]['comment_post_ID']) && $comments[$wpl_key]['comment_ID']) {
                    $url = get_permalink(intval($comments[$wpl_key]['comment_post_ID'])) . "#comment-" . trim($comments[$wpl_key]['comment_ID'], '#');
                    $comments[$wpl_key]['link'] = esc_url($url);
                    if (strlen($comments[$wpl_key]['comment_author']) > 15) {
                        $comments[$wpl_key]['comment_author'] = substr($comments[$wpl_key]['comment_author'], 0, 14) . "...";
                    }
                }
            }
            $wpLive_data['current_comments'] = $comments;
            $this->setWpLiveData($wpLive_data);
        }
    }

    /* Sending data to admin about updating users profile */

    function getUserProfileUpdate() {
        $userData = $this->dbLive->getUserProfileUpdate();
        if (!empty($userData)) {
            $wpLive_data['updated_profile'] = $userData;
            $wpLive_data['updated_profile']['count'] = count($userData);
            $this->setWpLiveData($wpLive_data);
        }
    }

    /* The array which collects data for sending js */

    function setWpLiveData($wpLive_data) {

        $this->wpLive_data = array_merge($this->wpLive_data, $wpLive_data);
    }

    /* Sending data for inserting in DB, about comments to the posts */

    function wpLiveNewPostComment($comment_id, $comment_object) {

        if ($comment_id) {
            if ($comment_object->comment_parent > 0) {
                $parent_comment = get_comment($comment_object->comment_parent);
                $user_id = $parent_comment->user_id;
                $this->auther = $user_id;
                $this->dbLive->insertLiveReply($user_id, $comment_object->user_id, $comment_id);
            } else {
                $comment_post_id = $comment_object->comment_post_ID;
                $post_data = get_post($comment_post_id);
                $this->auther = $comment_post_id;
                $this->dbLive->insertLiveComment($post_data->post_author, $comment_object->user_id, $comment_id);
            }
        }
    }

    /* Inserting data about user profile update in DB */

    function userProfileUpdate($user_id) {
        $this->dbLive->userProfileUpdate($user_id);
    }

    /* Getting Last post Information for sending to axaj */

    function getLastPosts() {
        $posts = $this->dbLive->getLastPosts();
        if ($posts) {
            $params['postTitle'] = $posts[0]['post_title'];
            $params['postSlug'] = $posts[0]['post_name'];
            $author = get_userdata($posts[0]['post_author']);
            $params['postAuthor'] = $author->display_name;
            $wpLive_data['post'] = $params;
            $this->setWpLiveData($wpLive_data);
        }
    }

    /* Getting comments and replies Information for sending to axaj */

    function getNewComments() {
        $comments = $this->dbLive->getUsersComments(get_current_user_id());

        if ($comments) {
            foreach ($comments as $wpl_key => $wpl_value) {
                if (isset($comments[$wpl_key]['comment_post_ID']) && $comments[$wpl_key]['comment_ID']) {
                    $url = get_permalink(intval($comments[$wpl_key]['comment_post_ID'])) . "#comment-" . trim($comments[$wpl_key]['comment_ID'], '#');
                    $comments[$wpl_key]['link'] = esc_url($url);
                    if (strlen($comments[$wpl_key]['comment_author']) > 15) {
                        $comments[$wpl_key]['comment_author'] = substr($comments[$wpl_key]['comment_author'], 0, 14) . "...";
                    }
                }
            }
            $wpLive_data['comments'] = $comments;
        }
        if (count($comments)) {
            $wpLive_data['comment_count'] = count($comments);

            $this->setWpLiveData($wpLive_data);
        }
    }

    public function addWpLiveMenu() {

        add_menu_page('wpLive', 'wpLive', 'manage_options', 'wplive', array($this, 'live_admin_page'));
    }

    public function pluginsLoaded() {

        $this->dbLive = new wpLiveDbManager();
        $this->dbLive->createWpLiveDB();
    }

    public function registerLiveAdminStyle() {
        wp_register_style('wplive-css', plugins_url(LIVE_DIR_NAME . '/resources/css/admin-page.css'), null);
        wp_enqueue_style('wplive-css');
        wp_register_style('font-awesome', plugins_url(LIVE_DIR_NAME . '/resources/font-awesome-4.5.0/css/font-awesome.min.css'), null, '4.4.0');
        wp_enqueue_style('font-awesome');

        wp_register_script('wplive-ajax-js', plugins_url(LIVE_DIR_NAME . '/resources/js/live_admin.js'), array('jquery'));
        wp_enqueue_script('wplive-ajax-js');
        wp_localize_script('wplive-ajax-js', 'wp_live_data', array('url' => admin_url('admin-ajax.php'), 'action' => 'getWpLiveData', 'interval' => $this->options->getInterval(),
        ));
        wp_localize_script('wplive-ajax-js', 'comment_seen', array('url' => admin_url('admin-ajax.php'), 'action' => 'commentSeen',));
        wp_localize_script('wplive-ajax-js', 'get_notes', array('url' => admin_url('admin-ajax.php'), 'action' => 'getNotifications',));
        wp_register_style('wplive-cp-index-css', plugins_url(LIVE_DIR_NAME . '/lib/colorpicker/css/index.css'));
        wp_enqueue_style('wplive-cp-index-css');
        wp_register_style('wplive-cp-compatibility-css', plugins_url(LIVE_DIR_NAME . '/lib/colorpicker/css/compatibility.css'));
        wp_enqueue_style('wplive-cp-compatibility-css');
        wp_register_script('wplive-cp-colors-js', plugins_url(LIVE_DIR_NAME . '/lib/colorpicker/js/colors.js'), array('jquery'), '1.0.0', false);
        wp_enqueue_script('wplive-cp-colors-js');
        wp_register_script('wplive-cp-colorpicker-js', plugins_url(LIVE_DIR_NAME . '/lib/colorpicker/js/jqColorPicker.min.js'), array('jquery'), '1.0.0', false);
        wp_enqueue_script('wplive-cp-colorpicker-js');
        wp_register_script('wplive-cp-index-js', plugins_url(LIVE_DIR_NAME . '/lib/colorpicker/js/index.js'), array('jquery'), '1.0.0', false);
        wp_enqueue_script('wplive-cp-index-js');
    }

    public function registerLiveFrontStyle() {
        wp_register_style('font-awesome', plugins_url(LIVE_DIR_NAME . '/resources/font-awesome-4.5.0/css/font-awesome.min.css'), null, '4.5.0');
        wp_enqueue_style('font-awesome');
        wp_register_style('wplive-css', plugins_url(LIVE_DIR_NAME . '/resources/css/front-page.css'), null);
        wp_enqueue_style('wplive-css');
        wp_register_script('wplive-ajax-front-js', plugins_url(LIVE_DIR_NAME . '/resources/js/live_front.js'), array('jquery'), '1.0.0', TRUE);
        wp_enqueue_script('wplive-ajax-front-js');
        //  wp_localize_script('wplive-ajax-front-js', 'find_all_posts', array('url' => admin_url('admin-ajax.php'), 'action' => 'getNewPosts', 'interval' => $this->options->getInterval(),));
        wp_localize_script('wplive-ajax-front-js', 'wp_live_data', array('url' => admin_url('admin-ajax.php'), 'action' => 'getWpLiveData', 'interval' => $this->options->getInterval(),));
        wp_localize_script('wplive-ajax-front-js', 'comment_seen', array('url' => admin_url('admin-ajax.php'), 'action' => 'commentSeen'));
        wp_localize_script('wplive-ajax-front-js', 'get_notes', array('url' => admin_url('admin-ajax.php'), 'action' => 'getNotifications'));
    }

    public function commentSeen() {
        global $wpdb;

        if (isset($_POST['wplive_comment_id'])) {
            $id = $_POST['wplive_comment_id'];
            $table = $wpdb->prefix . 'live_message';
            $sql = "UPDATE " . $table . " SET `seen`=1 WHERE `note_id`=" . $id;
            $wpdb->query($sql);
        }
        wp_die();
    }

    public function setOnlineDate($userId, $date) {

        if (!get_metadata('user', $userId, 'wplive_user_online_date')) {
            add_user_meta($userId, 'wplive_user_online_date', $date);
        } else {
            update_user_meta($userId, 'wplive_user_online_date', $date);
        }

        $this->online_date = $date;
        $this->userId = $userId;
    }

    public function get_online_date() {
        return $this->online_date;
    }

    public function get_user_id() {
        return $this->userId;
    }

    public function setAuther($auther) {
        $this->auther = $auther;
    }

    public function getAuther() {
        return $this->auther;
    }

    public function isUserOnline() {
        $current_time = time();
        $user_id = get_current_user_id();
        $user_online_date = get_user_meta($user_id, 'wplive_user_online_date');
        if ($user_online_date >= ( $current_time - $this->options->getInterval())) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function getOnlineUsers() {
        $this->showOnlineUsers = true;
        $online_users_info = array();
        $userdata = $this->dbLive->getOnlineUsers();
        $i = 0;
        if ($userdata->get_results()) {
            foreach ($userdata->get_results() as $value) {
                $online_users_info[$i] = $value->data;
                unset($online_users_info[$i]->user_login, $online_users_info[$i]->user_pass, $online_users_info[$i]->user_activation_key);
                $i++;
            }
            $wpLive_data['users']['data'] = $online_users_info;
            $wpLive_data['users']['count'] = count($online_users_info);
            $this->setWpLiveData($wpLive_data);
        }
    }

    public function getOnlineGuests() {
        $live_guests = $this->dbLive->getLiveGuests();
        if (!empty($live_guests)) {
            $i_ip = 0;
            $array = array();
            foreach ($live_guests as $ip) {
                $array[$i_ip] = $this->getIpDetails($ip->ip);
                $i_ip++;
            }
            $wpLive_data['guests'] = $array;
            $wpLive_data['guests']['count'] = count($array);
            $this->setWpLiveData($wpLive_data);
        }
    }

    public function getOnlineIps() {
        $liveUsers = $this->dbLive->getLiveIps();
        if (!empty($liveUsers)) {
            $i_ip = 0;
            $array = array();
            foreach ($liveUsers as $ip) {
                $array[$i_ip] = $this->getIpDetails($ip->ip);
                if (!is_null($ip->user_id)) {
                    $userData = get_user_by('id', $ip->user_id);
                    if (!is_null($userData)) {
                        $array[$i_ip]->user = $userData->display_name ? $userData->display_name : $userData->user_login;
                    }
                }
                $i_ip++;
            }
            $wpLive_data['map'] = $array;
            $wpLive_data['map']['count'] = count($array);
            $this->setWpLiveData($wpLive_data);
        }
    }

    function userRegistretion($user_id) {
        $user_ip = $this->get_guests_ip();
        $this->dbLive->insertRegisterUser($user_id, $user_ip, time());
    }

    function getNewUser() {
        $newusers = $this->dbLive->getNewUsers();
        if (!empty($newusers)) {
            $wpLive_data['newusers'] = $newusers;
            $this->setWpLiveData($wpLive_data);
        }
    }

    function showNotesPopup() {
        include_once 'view/front_view.php';
        include_once 'view/front_popup.php';
    }

    public function live_admin_page() {
        include_once 'view/wplive-admin-page.php';
    }

    function get_guests_ip() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        elseif (isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        elseif (isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        elseif (isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        elseif (isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    function getIpDetails($ip) {
        $details = json_decode(file_get_contents('http://geoip.nekudo.com/api/' . $ip));
        return $details;
    }

    /* Live View counts getters and setters */

    function getPostViews($postID) {
        $count_key = 'wpl_post_views_count';
        $count = get_post_meta($postID, $count_key, true);
        if ($count == '') {
            delete_post_meta($postID, $count_key);
            add_post_meta($postID, $count_key, '0');
            return "0";
        }
        $liveViewCount = $this->dbLive->getLiveViewCount($postID);
        if ($liveViewCount == null) {
            $liveViewCount = '0';
        }
        $views = array('viewCount' => $count, 'liveCount' => $liveViewCount[0]['live']);
        $this->setWpLiveData($views);
        return $count;
    }

    function setLiveViews($postID, $userID, $is_guest = 1) {

        if ($postID && $userID) {

            $this->dbLive->insertLiveViews($userID, $postID, $is_guest);
            $this->dbLive->deleteOldLiveViews();
        }
    }

    function my_the_post_action($content) {
        if (is_singular('post')) {
            $content = $content . '<div class="wpl_viewcount"><span id="wpl_post_view_count" data-id="' . get_the_ID() . '""></span> ' . $this->phrases->viewsNow . ': <span id="wpl_post_live_view_count"></span></div>';
        }
        return $content;
    }

    /* End Live View count */

    function hex2rgb($hex) {
        $hex = str_replace("#", "", $hex);

        if (strlen($hex) == 3) {
            $r = hexdec(substr($hex, 0, 1) . substr($hex, 0, 1));
            $g = hexdec(substr($hex, 1, 1) . substr($hex, 1, 1));
            $b = hexdec(substr($hex, 2, 1) . substr($hex, 2, 1));
        } else {
            $r = hexdec(substr($hex, 0, 2));
            $g = hexdec(substr($hex, 2, 2));
            $b = hexdec(substr($hex, 4, 2));
        }
        $rgb = array($r, $g, $b);
        //return implode(",", $rgb); // returns the rgb values separated by commas
        return $rgb; // returns an array with the rgb values
    }

}

$live = new wpLive();
