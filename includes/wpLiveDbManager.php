<?php

/**
 * Created by PhpStorm.
 * User: GEDEON
 * Date: 08/15/2016
 * Time: 12:58 PM
 */
class wpLiveDbManager {

    private $db;
    private $dbprefix;
    private $live_message;
    private $live_users;
    private $live_view_count;
    private $option;

    public function __construct() {
        global $wpdb;
        $this->option = new wpLiveOptions();
        $this->db = $wpdb;
        $this->dbprefix = $wpdb->prefix;
        $this->live_message = $this->dbprefix . 'live_message';
        $this->live_users = $this->dbprefix . 'live_users';
        $this->live_view_count = $this->dbprefix . 'live_view_count';
    }

    function createWpLiveDB() {
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

        if (!$this->isTableExists($this->live_message)) {
            $sql = "CREATE TABLE `" . $this->live_message . "`  ( `id` int(11) NOT NULL AUTO_INCREMENT,
                                  `from_user` int(11) DEFAULT NULL,
                                  `to_user` int(11) DEFAULT NULL,
                                  `note_id` int(11) DEFAULT NULL,
                                  `seen` int(1) DEFAULT NULL,
                                  `date` int(11) DEFAULT 0,
                                  `message_type` varchar(255) DEFAULT NULL,
                                  `icon` varchar(255) DEFAULT NULL,
                                  PRIMARY KEY (`id`)
                                ) ENGINE=MyISAM DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;";
            dbDelta($sql);
        }
        if (!$this->isTableExists($this->live_view_count)) {
            $sql = "CREATE TABLE `" . $this->live_view_count . "`  (
                                   `id` int(11) NOT NULL AUTO_INCREMENT,
                                  `user_id` varchar(255) ,
                                  `post_id` int(11) DEFAULT NULL,
                                  `date` int(11) DEFAULT 0,
                                  `is_guest` ENUM('0','1') DEFAULT '0',
                                  PRIMARY KEY (`id`)
                                ) ENGINE=MyISAM DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;";
            dbDelta($sql);
        }
        if (!$this->isTableExists($this->live_users)) {
            $sql = "CREATE TABLE `" . $this->live_users . "`  (
                                   `id` int(11) NOT NULL AUTO_INCREMENT,
                                  `user_id` int(11) DEFAULT NULL,
                                  `type` varchar(255) DEFAULT 'Guest',
                                  `ip` varchar(255) DEFAULT NULL UNIQUE,
                                  `date` int(11) DEFAULT 0,
                                  `seen` ENUM('0','1') DEFAULT '0',
                                  `event` ENUM('online','update') DEFAULT 'online',
                                  PRIMARY KEY (`id`)
                                ) ENGINE=MyISAM DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;";
            dbDelta($sql);
        }
    }

    public function isTableExists($tableName) {
        return $this->db->get_var("SHOW TABLES LIKE '$tableName'") == $tableName;
    }

     public function insertLiveViews($user_id, $post_id, $is_guest='1'){
         $curent_time = time();
         $sql=$this->db->prepare("INSERT INTO   `wp_live_view_count` (`user_id`,`post_id`,`date`, `is_guest`) 
                      VALUES('%s',".$post_id.",".$curent_time.",'".$is_guest."')",$user_id);
         $this->db->query($sql);
     }

    public function deleteOldLiveViews(){
        $deleteDate=time() - $this->option->getInterval()*5;
        $sql=$this->db->prepare("DELETE FROM `{$this->live_view_count}` WHERE `date`<%s",$deleteDate );
        $this->db->query($sql);
    }

    public  function getLiveViewCount($postID){
        $currentTime=time() - $this->option->getInterval();
        $sql = $this->db->get_results("SELECT COUNT(DISTINCT `user_id`) as live FROM `" . $this->live_view_count . "` WHERE `post_id`=".$postID." AND `date` > " .$currentTime." ;", ARRAY_A);
        return $sql;
    }

    public function getLastPosts() {
        $curent_time = time() - $this->option->getInterval();
        $sql = $this->db->get_results("SELECT * FROM `" . $this->dbprefix . "posts` WHERE `post_status` = 'publish' AND `post_type`='post' AND UNIX_TIMESTAMP(`post_date`)>" . $curent_time . "", ARRAY_A);
        return $sql;
    }

    public function addNewPosts($id, $autherId){
        $currentTime=time();
        $liveData = array('from_user' => $autherId, 'to_user' => null, 'note_id' => $id, 'message_type' => 'post', 'seen' => 0,'date'=>$currentTime);
        $this->db->insert($this->live_message, $liveData);
    }
    public function getNewPosts($autherId){
        $currentTime=time() - $this->option->getInterval()+2;
        $newPostsId=$this->db->get_results("SELECT p.`ID` as post_id FROM 
                                  `" . $this->live_message . "` AS m , `" . $this->dbprefix . "posts` AS p
                                  WHERE m.`seen`= 0 AND m.message_type='post' AND p.`ID`=m.`note_id` AND m.`from_user`!= " . $autherId." AND `date`>".$currentTime." ORDER BY p.`ID` LIMIT  10", ARRAY_A);
        return $newPostsId;
    }
//
//    public function getNewComments($autherId){
//        $currentTime=time() - $this->option->getInterval()+2;
//        $sql = $this->db->get_results("SELECT c.`comment_ID`,  c.`comment_author`, p.`post_title`,c.`comment_post_ID`, m.`message_type`, m.`date`
//                                  FROM `" . $this->dbprefix . "comments` AS c
//                                  INNER JOIN `" . $this->dbprefix . "live_message` AS m ON c.`comment_ID`=m.`note_id` , `" . $this->dbprefix . "posts` AS p
//                                  WHERE m.`seen`= 0 AND c.`comment_approved`=1 AND  p.ID=c.comment_post_ID
//                                  AND (m.message_type='Comment' OR m.message_type='Reply') AND m.`from_user`= " . $autherId . " AND m.`to_user` !=" . $autherId."
//                                   AND m.`date`>".$currentTime." ORDER BY m.`id` DESC LIMIT 10", ARRAY_A);
//        return $sql;
//    }

    public function deleteOldPosts(){
        $deleteDate=time() - $this->option->getInterval()*5;
        $sql=$this->db->prepare("DELETE FROM `{$this->live_message}` WHERE `date`<%s AND `message_type`='post'",$deleteDate );
        $this->db->query($sql);
    }
    public function insertLiveGuests($user_ip, $time) {
        $liveData = array('ip' => $user_ip, 'date' => $time);
        $sql = $this->db->insert($this->live_users, $liveData);
        return $sql;
    }

    public function updateLiveGuests($user_ip, $time) {
        $liveData = array('date' => $time);
        $sql = $this->db->update($this->live_users, $liveData, array('ip' => $user_ip));
        return $sql;
    }

    public function getLiveGuests() {
        $time = time() - $this->option->getInterval();
        $sql = $this->db->get_results("SELECT `ip` FROM `" . $this->live_users . "` WHERE `date`>={$time} AND `type`='Guest'");
        return $sql;
    }
	
    public function getLiveIps() {
        $time = time() - $this->option->getInterval();
        $sql = $this->db->get_results("SELECT `ip`, `user_id` FROM `" . $this->live_users . "` WHERE `date`>={$time}");
        return $sql;
    }

    public function insertLiveReply($from_user_id, $to_user_id, $replay_id) {
        $currentTime=time();
        $liveData = array('from_user' => $from_user_id, 'to_user' => $to_user_id, 'note_id' => $replay_id, 'message_type' => 'Reply', 'seen' => 0, 'date'=>$currentTime);
        $sql = $this->db->insert($this->live_message, $liveData);
        return $sql;
    }

    public function insertLiveComment($from_user_id, $to_user_id, $comment_id) {
        $currentTime=time();
        $liveData = array('from_user' => $from_user_id, 'to_user' => $to_user_id, 'note_id' => $comment_id, 'message_type' => 'Comment', 'seen' => 0, 'date'=>$currentTime);
        $sql = $this->db->insert($this->live_message, $liveData);
        return $sql;
    }

    public function updateCommentDate($comment_id){
        $currentTime=time();
        $liveData = array('date' => $currentTime);
        $this->db->update($this->live_message, $liveData, array('note_id' => $comment_id));
    }

    public function getUsersComments($user_id) {
        $sql = $this->db->get_results("SELECT c.`comment_ID`,  c.`comment_author`, p.`post_title`,c.`comment_post_ID`, m.`message_type`, m.`date`
                                  FROM `" . $this->dbprefix . "comments` AS c
                                  INNER JOIN `" . $this->dbprefix . "live_message` AS m ON c.`comment_ID`=m.`note_id` , `" . $this->dbprefix . "posts` AS p
                                  WHERE m.`seen`= 0 AND c.`comment_approved`=1 AND  p.ID=c.comment_post_ID 
                                  AND (m.message_type='Comment' OR m.message_type='Reply') AND m.`from_user`= " . $user_id . " AND m.`to_user` !=" . $user_id." ORDER BY m.`id` DESC", ARRAY_A);
        return $sql;
    }

    public function getUsersCurrentComments($user_id) {
        $sql = $this->db->get_results("SELECT c.`comment_ID`,  c.`comment_author`, p.`post_title`,c.`comment_post_ID`, m.`message_type`, m.`date`
                                  FROM `" . $this->dbprefix . "comments` AS c
                                  INNER JOIN `" . $this->dbprefix . "live_message` AS m ON c.`comment_ID`=m.`note_id` , `" . $this->dbprefix . "posts` AS p
                                  WHERE m.`seen`= 0 AND c.`comment_approved`=1 AND  p.ID=c.comment_post_ID 
                                  AND (m.message_type='Comment' OR m.message_type='Reply') AND m.`from_user`= " . $user_id . " AND m.`to_user` !=" . $user_id." AND m.`date`>UNIX_TIMESTAMP()-15 ORDER BY m.`id` DESC", ARRAY_A);
        return $sql;
    }

    public function getOnlineUsers() {
        $current_time = time();
        $interval = $this->option->getInterval();
        $result = new WP_User_Query(
                array('meta_query' => array(
                array(
                    'key' => 'wplive_user_online_date',
                    'value' => $current_time - $interval,
                    'type' => 'numeric',
                    'compare' => '>'))));

        return $result;
    }

    public function insertRegisterUser($user_id, $user_ip, $time) {
        $user_info = get_userdata($user_id);
        $rol = implode(', ', $user_info->roles);
        $getUserLiveID = $this->db->get_var("SELECT id FROM " . $this->live_users . " WHERE ip='" . $user_ip . "'");
        if ($getUserLiveID) {
            $liveData = array('user_id' => $user_id, 'type' => $rol, 'date' => $time);
            $this->db->update($this->live_users, $liveData, array('id' => $getUserLiveID));
        } else {
            $liveData = array('user_id' => $user_id, 'type' => $rol, 'ip' => $user_ip);
            $this->db->insert($this->live_users, $liveData);
        }
    }

    public function getNewUsers() {
        $newUsers = array();
        $result = $this->db->get_results("SELECT `user_id` FROM `" . $this->live_users . "` WHERE `type`!='Guest' AND `seen`='0' AND `event`='online' AND `user_id`!=" . get_current_user_id());
        $i = 0;
        foreach ($result as $value) {
            $newUsers[$i] = get_userdata($value->user_id);
            unset($newUsers[$i]->data->user_activation_key,$newUsers[$i]->data->user_pass,$newUsers[$i]->data->user_login);
            $i++;
        }
        return $newUsers;
    }

    public function userProfileUpdate($user_id) {
        $liveData = array('user_id' => $user_id, 'event' => 'update', 'seen' => '0');
        $this->db->update($this->live_users, $liveData, array('user_id' => $user_id));
    }

    public function getUserProfileUpdate() {
        $usersProfileUpdate = array();
        $result = $this->db->get_results("SELECT `user_id` FROM `" . $this->live_users . "` WHERE `type`!='Guest' AND `seen`='0' AND `event`='update' AND `user_id`!=" . get_current_user_id());
        $i = 0;
        foreach ($result as $value) {
            $usersProfileUpdate[$i] = get_userdata($value->user_id);
            $i++;
        }

        return $usersProfileUpdate;
    }

}
