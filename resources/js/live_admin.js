jQuery(document).ready(function ($) {

    $(document).delegate('#wpl-bell', 'click', function () {
        $('#wpl-notes').animate({top: 'toggle'}, 150);
        $('#triangle-down').animate({top: 'toggle'}, 150);
    });
    $(document).delegate('.wpl-replay-item > a', 'click', function () {
        var url = $(this).attr('href');
        var repl_id = url.match(/#comment-(\d+)/i)[1];
        commnet_seen(repl_id);
        var id_repl_exists = $("body").find('#comment-' + repl_id).length;
        if (id_repl_exists === 0) {
            window.location.reload(true);
        }
    });
    $(document).delegate('.wpl-comment-item > a', 'click', function () {
        var url = $(this).attr('href');
        var comm_id = url.match(/#comment-(\d+)/i)[1];
        commnet_seen(comm_id);
        var id_comm_exists = $("body").find('#comment-' + comm_id).length;
        if (id_comm_exists === 0) {
            window.location.reload(true);
        }
    });


    $(document).delegate('.wpl-notes', 'click', function () {
        var className=$(this).children('div:first-child').attr('class');

        $.ajax({
            type: "POST",
            url: get_notes.url,
            data: {
                action: get_notes.action,
                wplive_note_type: className
            },
            success: function (response) {
                location.reload();
            },
            error: function (errorThrown) {

            }

        });


    });

    if (location.href.indexOf('wplive') >= 0) {
        $('.wplive-color-picker').colorPicker();
    }

    $('.live_content').hide();
    var tab_id = 0;
    if (!(typeof getCookie('live_tab_id') === 'undefined')) {
        tab_id = getCookie('live_tab_id');
    }
    $('.admin_menu li').removeClass("active_tab");
    $('[data-id="' + tab_id + '"]').addClass("active_tab");
    $('.live_content[data-id="' + tab_id + '"]').show();
    $('#wplive_reset_map').click(function () {
        wpLive_start = 0;
    });
    function getCookie(name) {
        var matches = document.cookie.match(new RegExp(
                "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
                ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }
    $(document).delegate('.admin_menu li', 'click', function () {
        var id = $(this).attr("data-id");
        document.cookie = "live_tab_id=" + id;
        $('.live_content').hide();
        $('.admin_menu li').removeClass("active_tab");
        $(this).addClass("active_tab");
        $('.live_content[data-id="' + getCookie('live_tab_id') + '"]').show();
    });

    var count = localStorage.getItem('note_count');
    if(!count){
        count=0;
    }
    $(document).delegate('.wpl-notes', 'click', function () {
        var className=$(this).children('div:first-child').attr('class');
        var notes = localStorage.getItem('note_count');

        // var disable = $(this).children('i').hasClass('disabled');
        if($(this).children('i').hasClass('wpl-disabled')){
            count=-1;
            $(this).children('i').removeClass('wpl-disabled');
        }else {
            count=1;
            $(this).children('i').addClass('wpl-disabled')
        }

        $.ajax({
            type: "POST",
            url: get_notes.url,
            data: {
                action: get_notes.action,
                wplive_note_type: className
            },
            success: function (response) {

                if(notes ){
                    console.log(count);
                    localStorage.setItem('note_count', (parseInt(notes)+count));
                }else{
                    localStorage.setItem('note_count', 1);
                }
                // localStorage.setItem('note_count', );
                location.reload();


                //TODO create object in local storage and keep the classes on this object. And then check
                // if class count is full dont call ajax.
            },
            error: function (errorThrown) {
            }

        });


    });
    var note_class=$(".wpl-notes").length;
    if(note_class!=count){
        getAjax();
        $.ajax({
            type: "POST",
            url: wp_live_data.url,
            success: function (response) {
                var interval = parseInt(wp_live_data.interval) * 1000;
                setInterval(function () {
                    getAjax();
                }, interval);
            },
            error: function (errorThrown) {
            }
        });
    }

    function getAjax() {
        var wpl_post_id = $("#wpl_post_view_count").attr("data-id");
        $.ajax({
            type: "POST",
            url: wp_live_data.url,
            data: {
                action: wp_live_data.action,
                wpl_post_id: wpl_post_id

            },
            success: function (response) {
                var params = $.parseJSON(response);
                console.log(params);
                if (params !== null && wpLive_start == 1) {
                    initMap(params);
                }
                if (params !== null) {
                    getNewPosts(params);

                    if (params.comments || params.replay) {
                        showCommentData(params);
                    }

                    if (params.users) {
                        showOnlineUsers(params);
                    }

                    if (params.newusers) {
                        showNewUsers(params);
                    }

                    newCommnentCount(params);
                    onlineUsers(params);
                    guestsCount(params);
                }
            }
        });
    }
    function onlineUsers(usersdata) {
        var user=  $('#wpl_users');
        if (usersdata.users) {
            user.text(usersdata.users.count);
            user.css('display', 'block');
        } else {
            user.text(0);
            user.hide();
        }
    }

    function hideCommentData() {
        $('.wpl-commnts-list').remove();
    }

    function scrollTo() {
        var comment_url = window.location.href;

        if (comment_url.indexOf('#comment-') !== -1) {
            var id = comment_url.match(/#comment-(\d+)/i)[1];

            $(window).load(function () {
                $('html,body').animate({scrollTop: $('#comment-' + id).offset().top}, 'slow');
            });
        }
    }

    function commnet_seen(comment_id) {

        var id = comment_id;

        $.ajax({
            type: "POST",
            url: comment_seen.url,
            data: {
                action: comment_seen.action,
                wplive_comment_id: id
            },
            success: function (response) {
            },
            error: function (errorThrown) {
            }

        });
    }

    var countAll;
    function newCommnentCount(parameters) {
        var count =$('#wpl_commnets_count');
        if (parameters.comment_count) {

            count.text(parameters.comment_count);
            countAll = parameters.comment_count;
            $(".count-all").text(countAll);
            count.css('display', 'block');
        } else {
            count.text(0);
            count.hide();

        }
    }

    function guestsCount(parameters) {
        var guests=$('#wpl_guest');
        if (parameters.guests) {
            guests.text(parameters.guests.count);
            guests.css('display', 'block');
        } else {
            guests.text(0);
            guests.hide();
        }
    }
    scrollTo();
    $(".count-all").text(countAll);
    function notificationAll(count) {
        var notif_bar = $(".wplive-button >i");
        if (notif_bar.hasClass("open")) {
            notif_bar.removeClass("open");
            notif_bar.addClass('closed');
            $(this).attr('title', "Open Live Bar");
            $(".wplive_front_bar").animate({width: '0'}, 400);
            $(".count-all").css('display', 'block');
            $(".count-all").text(count);
        } else {
            notif_bar.removeClass("closed");
            notif_bar.addClass('open');
            $(this).attr('title', "Close Live Bar");
            $(".wplive_front_bar").animate({width: '50px'}, 400);
            $(".count-all").css('display', 'block');
            $(".count-all").text(count);
        }
    }
    $(".wplive-button").click(function () {


        notificationAll(countAll);
    });

});
var wpLive_start = 0;
function initMap(obj) {

    if (wpLive_start == 1) {
        var map, lat, lng, loc;
        map = new google.maps.Map(document.getElementById('map'), {
        });
        var bounds = new google.maps.LatLngBounds();
        var userName = '';
        var gusetCount=1;
        if (obj.map != null) {
            for (var k = 0; k < obj.map['count']; k++) {
                if (obj.map[k] != null) {
                    if (obj.map[k].type != 'error') {
                        lat = parseFloat(obj.map[k].location.latitude);
                        lng = parseFloat(obj.map[k].location.longitude);
                        obj.map[k].loc = {lat: lat, lng: lng};
                        loc = new google.maps.LatLng(lat, lng);
                        bounds.extend(loc);
                        if (obj.map[k].user) {
                            userName = "<b>User Name: </b> <i>" + obj.map[k].user.toString() + "</i></br>";
                        }else{
                            userName = "<b>User Name: </b> <i> Guest-" + gusetCount + "</i></br>";
                            gusetCount++;
                        }
                        createMarker(map, obj.map[k].loc, obj.map[k].city.toString(), userName + "<b>Countri: </b> <i>" + obj.map[k].country.name.toString() + "</i></br><b>City: </b> <i>" + obj.map[k].city.toString() + "</i>", false);                       
                    }
                }
            }
        }
        map.fitBounds(bounds);
        map.panToBounds(bounds);
    } else {
        document.getElementById('map').innerHTML = "<div id='map_loadding'><img  src='../wp-content/plugins/wplive/resources/img/loading.gif'/></div>";
    }
    if (wpLive_start < 2) {
        wpLive_start++;
    }
}

function createMarker(map, markerPos, markerTitle, infoWindowContent, displayInfoWindow) {

    var marker = new google.maps.Marker({
        position: markerPos,
        map: map,
        title: markerTitle
    });

    var infowindow = new google.maps.InfoWindow({
        content: infoWindowContent
    });

    if (displayInfoWindow) {
        infowindow.open(map, marker);
    }
    google.maps.event.addListener(marker, 'click', function () {
        infowindow.open(map, marker);
    });
}

