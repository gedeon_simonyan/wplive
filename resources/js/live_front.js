jQuery(document).ready(function ($) {

    $(document).delegate('#wpl-bell', 'click', function () {
        $('#wpl-notes').animate({top: 'toggle'}, 150);
        $('#triangle-down').animate({top: 'toggle'}, 150);
    });

    $(document).delegate('.wpl-replay-item > a', 'click', function () {
        var url = $(this).attr('href');
        var repl_id = url.match(/#comment-(\d+)/i)[1];
        commnet_seen(repl_id);
        var id_repl_exists = $("body").find('#comment-' + repl_id).length;
        if (id_repl_exists === 0) {
            window.location.reload(true);
        }
    });

    $(document).delegate('.wpl-comment-item > a', 'click', function () {
        var url = $(this).attr('href');
        var comm_id = url.match(/#comment-(\d+)/i)[1];
        commnet_seen(comm_id);
        var id_comm_exists = $("body").find('#comment-' + comm_id).length;
        if (id_comm_exists === 0) {
            window.location.reload(true);
        }
    });
    var count = localStorage.getItem('note_count');
    if(!count){
        count=0;
    }
    $(document).delegate('.wpl-notes', 'click', function () {
        var className=$(this).children('div:first-child').attr('class');
        var notes = localStorage.getItem('note_count');

        // var disable = $(this).children('i').hasClass('disabled');
        if($(this).children('i').hasClass('wpl-disabled')){
            count=-1;
            $(this).children('i').removeClass('wpl-disabled');
        }else {
            count=1;
            $(this).children('i').addClass('wpl-disabled')
        }

        $.ajax({
            type: "POST",
            url: get_notes.url,
            data: {
                action: get_notes.action,
                wplive_note_type: className
            },
            success: function (response) {

                if(notes ){
                    console.log(count);
                    localStorage.setItem('note_count', (parseInt(notes)+count));
                }else{
                    localStorage.setItem('note_count', 1);
                }
                // localStorage.setItem('note_count', );
                location.reload();


                //TODO create object in local storage and keep the classes on this object. And then check
                // if class count is full dont call ajax.
            },
            error: function (errorThrown) {
            }

        });


    });
    var note_class=$(".wpl-notes").length;
    if(note_class!=count){
        getAjax();
        $.ajax({
            type: "POST",
            url: wp_live_data.url,
            success: function (response) {
                var interval = parseInt(wp_live_data.interval) * 1000;
                setInterval(function () {
                    getAjax();
                }, interval);
            },
            error: function (errorThrown) {
            }
        });
    }



    function drawViewCount(data) {
        var liveCount = data.liveCount;
        $("#wpl_post_live_view_count").text(liveCount);

    }
    function getAjax() {
        var wpl_post_id = $("#wpl_post_view_count").attr("data-id");
        $.ajax({
            type: "POST",
            url: wp_live_data.url,
            data: {
                action: wp_live_data.action,
                wpl_post_id: wpl_post_id
            },
            success: function (response) {
                var params = $.parseJSON(response);
                console.log(params);
                if (params !== null) {
                    getNewPosts(params);

                    if (params.comments || params.replay) {
                        showCommentData(params);
                    }

                    if (params.users) {
                        showOnlineUsers(params);
                    }

                    if (params.newusers) {
                        showNewUsers(params);
                    }

                    newCommnentCount(params);
                    onlineUsers(params);
                    guestsCount(params);
                    drawViewCount(params);
                }
            },
            error: function (errorThrown) {
            }

        });
    }
    function onlineUsers(usersdata) {
        var user=  $('#wpl_users');
        if (usersdata.users) {
           user.text(usersdata.users.count);
            user.css('display', 'block');
        } else {
            user.text(0);
            user.hide();
        }
    }

    function hideCommentData() {
        $('.wpl-commnts-list').remove();
    }

    function scrollTo() {
        var comment_url = window.location.href;

        if (comment_url.indexOf('#comment-') !== -1) {
            var id = comment_url.match(/#comment-(\d+)/i)[1];

            $(window).load(function () {
                $('html,body').animate({scrollTop: $('#comment-' + id).offset().top}, 'slow');
            });
        }
    }

    function commnet_seen(comment_id) {

        var id = comment_id;

        $.ajax({
            type: "POST",
            url: comment_seen.url,
            data: {
                action: comment_seen.action,
                wplive_comment_id: id
            },
            success: function (response) {
            },
            error: function (errorThrown) {
            }

        });
    }
    var countAll;
    function newCommnentCount(parameters) {
        var count =$('#wpl_commnets_count');
        if (parameters.comment_count) {

            count.text(parameters.comment_count);
            countAll = parameters.comment_count;
            $(".count-all").text(countAll);
            count.css('display', 'block');
        } else {
            count.text(0);
            count.hide();

        }
    }

    function guestsCount(parameters) {
        var guests=$('#wpl_guest');
        if (parameters.guests) {
            guests.text(parameters.guests.count);
            guests.css('display', 'block');
        } else {
            guests.text(0);
            guests.hide();
        }
    }
    scrollTo();
    $(".count-all").text(countAll);
    function notificationAll(count) {
        var notif_bar = $(".wplive-button >i");
        if (notif_bar.hasClass("open")) {
            notif_bar.removeClass("open");
            notif_bar.addClass('closed');
            $(this).attr('title', "Open Live Bar");
            $(".wplive_front_bar").animate({width: '0'}, 400);
            $(".count-all").css('display','block');
            $(".count-all").text(count);
        } else {
            notif_bar.removeClass("closed");
            notif_bar.addClass('open');
            $(this).attr('title', "Close Live Bar");
            $(".wplive_front_bar").animate({width: '50px'}, 400);
            $(".count-all").css('display','block');
            $(".count-all").text(count);
        }
    }

    $(".wplive-button").click(function () {


        notificationAll(countAll);
    });

});

